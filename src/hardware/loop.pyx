# distutils: language = c++   
import select
import socket

from loop cimport SerialUSB

cdef class Loop:
    cdef SerialUSB* c_serial

    def __init__(self):
        self.c_serial = new SerialUSB()

    def raw_write(self,msg):
        self.c_serial.write(msg,len(msg))

    def stop_run(self):
        self.c_serial.stop_run()
        
    def run(self,int port):
        self.c_serial.run(port)
 
