#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
 #include <arpa/inet.h>
#include <poll.h>
#include <signal.h>
#include <sys/signalfd.h>
#include "usb.h"

#define METTLER_ID 0x0eb8
#define PRODUCT_ID 0xe900


const static unsigned char endpoint_in=0x81; /* endpoint 0x81 address for IN */
const static unsigned char endpoint_out=2; /* endpoint 2 address for OUT */

extern "C"
{
  static void _read_cbk(struct libusb_transfer *transfer)
  {
    SerialUSB* klass = (SerialUSB*)transfer->user_data;
    klass->read_cbk(transfer);
  }
  static void _added_fd_cbk(int fd,short events,void *user_data)
  {
    SerialUSB* klass = (SerialUSB*)user_data;
    klass->_insert_fd(fd,events);
  }
  static void _remove_fd_cbk(int fd,void *user_data)
  {
    SerialUSB* klass = (SerialUSB*)user_data;
    klass->_remove_fd(fd);
  }
}

SerialUSB::SerialUSB() : m_ctx(NULL),m_handle(NULL),m_transfer(NULL),m_read_buffer(NULL),
			 m_client_socket(-1),m_read_pipe(-1),m_write_pipe(-1)
{
  int return_flag = libusb_init(&m_ctx);
  if(return_flag < 0)
    throw std::runtime_error("Can not initialize libusb");
  //install pollfd notifiers
  libusb_set_pollfd_notifiers(m_ctx, _added_fd_cbk, _remove_fd_cbk, this);
  
  libusb_set_option(m_ctx,LIBUSB_OPTION_LOG_LEVEL,3);

  m_handle = libusb_open_device_with_vid_pid(m_ctx, METTLER_ID, PRODUCT_ID);
  if(!m_handle)
    {
      //Check if we can see the device on the USB bus
      bool found = false;
      libusb_device **devs;
      int cnt = libusb_get_device_list(m_ctx, &devs);
      for(int i = 0;!found && i < cnt;++i)
	{
	  libusb_device *dev = devs[i];
	  libusb_device_descriptor desc;
	  int r = libusb_get_device_descriptor(dev, &desc);
	  if(r < 0) continue;
	      
	  found = (desc.idVendor == METTLER_ID &&
		   desc.idProduct == PRODUCT_ID);
	}
      libusb_free_device_list(devs, 1);
      
      if(found)
	throw std::runtime_error("Device is found on the Bus but cannot be open "
				 "(Check the permission)");
      else
	throw std::runtime_error("Device not found is it plugged?");
    }
  // Init Asynchronous read
  m_transfer = libusb_alloc_transfer(0);
  if(!m_transfer)
    throw std::runtime_error("Can not allocate usb transfer");

  int BUFFER_SIZE = getpagesize();
  if(posix_memalign(&m_read_buffer,BUFFER_SIZE,BUFFER_SIZE))
    throw std::runtime_error("Can not allocate usb buffer");
    
  libusb_fill_bulk_transfer(m_transfer,m_handle,
			    endpoint_in,
			    (unsigned char*)m_read_buffer,BUFFER_SIZE,
			    _read_cbk,
			    this,0);
  _submit_transfer();
  int pipe_fd[2];
  if(pipe(pipe_fd))
    throw std::runtime_error("Can not create pipe");

  m_read_pipe = pipe_fd[0];
  m_write_pipe = pipe_fd[1];
  _insert_fd(m_read_pipe,POLLIN);
}

SerialUSB::~SerialUSB()
{
  //remove notifiers
  libusb_set_pollfd_notifiers(m_ctx, NULL, NULL, this);

  if(m_ctx)
    libusb_exit(m_ctx);
  if(m_handle)
    libusb_close(m_handle);
  if(m_transfer)
    libusb_free_transfer(m_transfer);
  if(m_read_buffer)
    free(m_read_buffer);
  if(m_read_pipe > -1)
    close(m_read_pipe);
  if(m_write_pipe > -1)
    close(m_write_pipe);
}

void SerialUSB::write(const char* msg,int lenght)
{

  int ret = libusb_bulk_transfer(m_handle,endpoint_out,
				 (unsigned char*)msg,lenght,
				 NULL,100);
  if(ret)
    {
      switch(ret)
	{
	case LIBUSB_ERROR_TIMEOUT: std::runtime_error("raw_write timeout");break;
	case LIBUSB_ERROR_NO_DEVICE: std::runtime_error("raw_write no device");break;
	case LIBUSB_ERROR_PIPE: std::runtime_error("raw_write endpoint halted");break;
	default: std::runtime_error("raw_write unknown error");break;
	}
    }
}

void SerialUSB::_submit_transfer()
{  
  int error = libusb_submit_transfer(m_transfer);
  if(error)
    {
      printf("_submit_transfer %d\n",error);
      switch(error)
	{
	case LIBUSB_ERROR_BUSY: std::runtime_error("transfer: already submitted");break;
	case LIBUSB_ERROR_NO_DEVICE: std::runtime_error("transfer: no device");break;
	case LIBUSB_ERROR_NOT_SUPPORTED: std::runtime_error("transfer: not supported");break;
	case LIBUSB_ERROR_INVALID_PARAM: std::runtime_error("transfer: invalid parameters");break;
	default: std::runtime_error("transfer: unknown error");break;
	}
    }
}

void SerialUSB::read_cbk(struct libusb_transfer *transfer)
{
  if(m_client_socket > -1 && transfer->actual_length > 0)
    ::write(m_client_socket,transfer->buffer,transfer->actual_length);
  _submit_transfer();
}

void SerialUSB::stop_run()
{
  m_run_flag = false;
  ::write(m_write_pipe,"|",1);
}

void SerialUSB::run(int port)
{
  int server_socket = socket(AF_INET, SOCK_STREAM, 0);
  int option = 1;
  if(setsockopt(server_socket,SOL_SOCKET,SO_REUSEADDR,&option,sizeof(option)))
    std::runtime_error("Can not set option on server socket");
  struct sockaddr_in server_address;
  server_address.sin_family = AF_INET;
  server_address.sin_addr.s_addr = htonl(INADDR_ANY);
  server_address.sin_port = htons(port);
  if(bind(server_socket,
	  (struct sockaddr*)&server_address,
	  sizeof(server_address)))
    std::runtime_error("Could not bin the server to port");
  if(listen(server_socket,1))
    std::runtime_error("Could not listen on socket");

  struct pollfd *fds = NULL;
  int nb_fd = 0;

  _insert_fd(server_socket,POLLIN);
  m_run_flag = true;
  while(m_run_flag)
    {
      if(m_fds_dirty)		// Rebuild fds array
	{
	  nb_fd = 0;
	  fds = (struct pollfd*)realloc(fds,sizeof(pollfd)*m_fds.size());
	  for(auto& [key, value]: m_fds)
	    {
	      int i = nb_fd++;
	      fds[i].fd = key;
	      fds[i].events = value.events;
	    }
	  m_fds_dirty = false;
	}
      int timeout = -1;
      struct timeval tv;
      if(libusb_get_next_timeout(m_ctx,&tv)) // calculate the timeout if any
	timeout = tv.tv_sec * 1e3 + tv.tv_usec / 1e3;

      
      struct timespec before,after;
      clock_gettime(CLOCK_MONOTONIC,&before);
      int event_nb = poll(fds,nb_fd,timeout);
      if(event_nb == -1 && errno == EINTR)	// check if some signal was emit
	break;
      clock_gettime(CLOCK_MONOTONIC,&after);


      bool handle_usb_event_flag = false;
      if(event_nb > 0)
	{
	  for(int i = 0;i < nb_fd && event_nb;++i)
	    {
	      if(!fds[i].revents) continue;

	      --event_nb;
	      if(fds[i].fd == server_socket)
		{
		  if(m_client_socket > 0)
		    {
		      //kickout the previous client
		      close(m_client_socket);
		      _remove_fd(m_client_socket);
		    }
		  struct sockaddr_in client_address;
		  socklen_t len = sizeof(client_address);
		  
		  m_client_socket = accept(server_socket,
					 (struct sockaddr*)&client_address, &len);
		  if(m_client_socket)
		    _insert_fd(m_client_socket,POLLIN);
		}
	      else if(fds[i].fd == m_client_socket)
		{
		  char message[1024];
		  int receive_bytes = ::read(m_client_socket,message,sizeof(message));
		  message[receive_bytes] = '\0';
		  if(!receive_bytes)
		    {
		      close(m_client_socket);
		      _remove_fd(m_client_socket);
		      m_client_socket = -1;
		    }
		  else
		    this->write(message,receive_bytes);
		}
	      else if(fds[i].fd == m_read_pipe)
		{
		  //just need to empty pipe
		  char message[1024];
		  ::read(m_read_pipe,message,sizeof(message));
		}
	      else		// some usb events need to be handle
		handle_usb_event_flag = true;
	    }
	}
      
      if(handle_usb_event_flag)
	{
	  tv = {0,0};
	  if(libusb_handle_events_timeout(m_ctx,&tv))
	    {
	      std::cerr << "Error on usb handle events" << std::endl;
	      break;		// for now quit the loop
	    }
	}
      if(timeout >= 0)
	{
	  if(after.tv_nsec < before.tv_nsec)
	    {
	      int nsec = (before.tv_nsec - after.tv_nsec) / int(1e9) + 1;
	      before.tv_nsec -= int(1e9) * nsec;
	      before.tv_sec += nsec;
	    }
	  double elapsed_time = before.tv_sec - after.tv_sec +
	    (before.tv_nsec - after.tv_nsec) * 1e-9;
	  if(elapsed_time * 1e3 >= timeout)
	    {
	      tv = {0,0};
	      if(libusb_handle_events_timeout(m_ctx,&tv))
		{
		  std::cerr << "Error on usb handle events timeout" << std::endl;
		  break;		// for now quit the loop
		}
	    }
	}
    }
  if(fds)
    free(fds);
  if(m_client_socket > 0)
    {
      close(m_client_socket);
      m_client_socket = -1;
    }
  close(server_socket);
}

void SerialUSB::_remove_fd(int fd)
{
  m_fds.erase(fd);
  m_fds_dirty = true;
}

void SerialUSB::_insert_fd(int fd, short event)
{
  std::pair<int,struct pollfd> value(fd,{fd,event,0});
  m_fds.insert(value);
  m_fds_dirty = true;
}
