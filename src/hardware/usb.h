#include <iostream>
#include <map>
#include <libusb-1.0/libusb.h> /* libusb header */

class SerialUSB
{
 public:
  SerialUSB();
  ~SerialUSB();
  
  void write(const char *msg,int lenght);
  void read_cbk(struct libusb_transfer*);
  void run(int port);
  void stop_run();
  
  void _remove_fd(int fd);
  void _insert_fd(int fd, short events);
private:
  void _submit_transfer();

  
  libusb_context*		m_ctx;
  libusb_device_handle*		m_handle;
  libusb_transfer*		m_transfer;
  void*				m_read_buffer;
  std::map<int,struct pollfd>	m_fds;
  bool				m_fds_dirty;
  bool				m_run_flag;
  int				m_client_socket;
  int				m_read_pipe;
  int				m_write_pipe;
};
