# distutils: language = c++
from libcpp import bool

cdef extern from "usb.h":
    cdef cppclass SerialUSB:
        SerialUSB() except +
        void write(const char *msg,int lenght) nogil except +
        void stop_run() nogil
        void run(int port) nogil except +
