from setuptools.extension import Extension
from setuptools import setup


hardware = Extension("mettler_serial.hardware",
                     sources=["src/hardware/usb.cpp","src/hardware/loop.pyx"],
                     language="c++",
                     libraries=['usb-1.0'])
def main():
    setup(name='mettler serial',
          author='seb',
          version='1.0.0',
          description='Mettler serial over tcp',
          package_dir={"mettler_serial": "mettler_serial"},
          packages=["mettler_serial"],
          ext_modules=[hardware],
          install_requires=['cython'],
          entry_points={"console_scripts":["mettler_serial = mettler_serial.server:main"]},
          )


if __name__ == "__main__":
    main()
      
