import argparse
import signal
from .hardware import Loop

def main():
    parser = argparse.ArgumentParser(description="Serial over tcp for mettler toledo")
    parser.add_argument('--port',dest="port",type=int,default=8000)
    args = parser.parse_args()
    l = Loop()
    l.run(args.port)
